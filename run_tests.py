#!/usr/bin/env python3

import os
import re
import sys
import unittest

TEST_DIR = os.path.join('tests')

# Return a list of unittest.TestCase
def collect_tests(suite, test_cases = []):
    if (isinstance(suite, unittest.TestCase)):
        test_cases.append(suite)
        return test_cases

    if (not isinstance(suite, unittest.suite.TestSuite)):
        raise ValueError("Unknown test type: %s" % (str(type(suite))))

    for test_object in suite:
        collect_tests(test_object, test_cases)

    return test_cases

def main(pattern = None):
    runner = unittest.TextTestRunner(verbosity = 3)
    discovered_suite = unittest.TestLoader().discover(TEST_DIR)
    test_cases = collect_tests(discovered_suite)

    tests = unittest.suite.TestSuite()

    for test_case in test_cases:
        if (pattern is None or re.search(pattern, test_case.id())):
            tests.addTest(test_case)
        else:
            print("Skipping %s because of match pattern." % (test_case.id()))

    if not runner.run(tests).wasSuccessful():
        sys.exit(1)

if __name__ == '__main__':
    if (len(sys.argv) > 2):
        print("USAGE: python %s [test pattern]" % (sys.argv[0]))
        sys.exit(1)

    pattern = None
    if (len(sys.argv) == 2):
        pattern = sys.argv[1]

    main(pattern)
