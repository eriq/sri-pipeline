# Pipeline Runtime

A simple runtime for running D3M pipelines.
Originally taken from Diego's pipeline code: https://gitlab.com/datadrivendiscovery/d3m/blob/runtime/d3m/runtime.py

## Installing

You can install from source, or from pip:
```
pip3 install sripipeline
```

## Command Line Tool

You can invoke the pipeline tool via the command line with:
```
python -m sripipeline.runtime <pipeline path> <dataset path>
```

Get help with:
```
python -m sripipeline.runtime --help
```
