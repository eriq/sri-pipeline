import os
import pickle
import unittest

from sripipeline import runtime

PIPELINE_PATH = os.path.join('tests', 'pipelines', 'graph_matching.json')

BASE_DATASET_DIR = os.path.join('..', 'data', 'seed_datasets_current', '49_facebook')
TRAIN_DATASET_DOC = os.path.join(BASE_DATASET_DIR, 'TRAIN', 'dataset_TRAIN', 'datasetDoc.json')
TRAIN_PROBLEM_DOC = os.path.join(BASE_DATASET_DIR, 'TRAIN', 'problem_TRAIN', 'problemDoc.json')
TEST_DATASET_DOC = os.path.join(BASE_DATASET_DIR, 'TEST', 'dataset_TEST', 'datasetDoc.json')

# @unittest.skip('')
class TestGraphMatching(unittest.TestCase):
    def test_run(self):
        pipeline_runtime = runtime.generate_pipeline(
                pipeline_path = PIPELINE_PATH,
                dataset_path = TRAIN_DATASET_DOC,
                problem_doc_path = TRAIN_PROBLEM_DOC)

        results = runtime.test_pipeline(pipeline_runtime, TEST_DATASET_DOC)

        self.assertEquals(len(results), 1)
        self.assertEquals(len(results[0]), 3)

    def test_pickle(self):
        pipeline_runtime = runtime.generate_pipeline(
                pipeline_path = PIPELINE_PATH,
                dataset_path = TRAIN_DATASET_DOC,
                problem_doc_path = TRAIN_PROBLEM_DOC)

        pickle_string = pickle.dumps(pipeline_runtime)
        new_pipeline_runtime = pickle.loads(pickle_string)

        results = runtime.test_pipeline(new_pipeline_runtime, TEST_DATASET_DOC)

        self.assertEquals(len(results), 1)
        self.assertEquals(len(results[0]), 3)

if __name__ == '__main__':
    unittest.main()
